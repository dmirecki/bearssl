#include "inner.h"
#include <time.h>
#include <stdlib.h>

#define CHECK_BIT(var, pos) ((var) & (1<<(pos)))
#define SET_BIT(var, pos) (var |= (1<<(pos)))

unsigned char temp[128];
unsigned char permutation[128];
unsigned char inverse_permutation[128];
unsigned char temp_buf[16];

uint32_t hamming_weight_char(unsigned char x) {
    uint32_t count;
    for (count = 0; x; count++)
        x &= x - 1;
    return count;
}

uint32_t hamming_weight(uint32_t x) {
    uint32_t count;
    for (count = 0; x; count++)
        x &= x - 1;
    return count;
}

void riffle(void *data) {

    unsigned char *buf = data;

    memset(temp_buf, 0, sizeof(temp_buf));


    for (uint32_t i = 0; i < 128; i++) {
        if (CHECK_BIT(buf[i >> 3], i & 7)) {
            SET_BIT(temp_buf[permutation[i] >> 3], permutation[i] & 7);
        }
    }

    memcpy(buf, temp_buf, 16);
}

void unriffle(void *data) {

    unsigned char *buf = data;

    memset(temp_buf, 0, sizeof(temp_buf));

    for (uint32_t i = 0; i < 128; i++) {
        if (CHECK_BIT(buf[i >> 3], i & 7)) {
            SET_BIT(temp_buf[inverse_permutation[i] >> 3], inverse_permutation[i] & 7);
        }
    }

    memcpy(buf, temp_buf, 16);
}

void create_r(unsigned char *r) {

    for (uint32_t i = 0; i < 16; i++) {
        r[i] = i << 3;
    }
}

void create_permutation(unsigned char *r) {

    uint32_t permutation_key[44];
    uint32_t *key = permutation_key;

    void *r_ptr = r;
    br_aes_keysched(key, r_ptr, 16);

    key += 4; /*There is r on the first 4 bytes */

    for (uint32_t i = 0; i < 128; i++) {
        permutation[i] = i;
    }

    uint32_t k0, k1;

    for (uint32_t i = 0; i < 8; i++) {

        k0 = 0;
        k1 = 128;

        for (uint32_t k = 0; k < 4; k++) {
            k1 -= hamming_weight(key[k]);
        }

        for (uint32_t j = 0; j < 128; j++) {
            if (CHECK_BIT(key[j / 32], j % 32)) {
                temp[k1] = permutation[j];
                k1++;
            } else {
                temp[k0] = permutation[j];
                k0++;
            }
        }

        for (uint32_t j = 0; j < 128; j++) {
            permutation[j] = temp[j];
        }

        key += 4;
    }

    for (int i = 0; i < 128; i++) {
        inverse_permutation[permutation[i]] = i;
    }
}